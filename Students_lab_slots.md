| Roll No | Name                                | Day       | TA                 |
|---------|-------------------------------------|-----------|--------------------|
| 11163   | ASHOK KUMAR                         | Monday    | Susmit Wagle       |
| 12001   | AAHITAGNI MUKHERJEE                 | Monday    | Abhishek Rose      |
| 12098   | AMIT KUMAR MEENA                    | Monday    |                    |
| 12385   | M ARVIND                            | Wednesday | Debjeet Majumdar   |
| 12657   | SHANU VASHISHTHA                    | Wednesday |                    |
| 12750   | SWATHI KRISHNA D                    | Wednesday | Debjeet Majumdar   |
| 13013   | ABDUS SAMAD                         | Monday    |                    |
| 13027   | ABHISHEK JAIN                       | Monday    | Abhishek Rose      |
| 13167   | ASIM UNMESH                         | Monday    | Abhishek Rose      |
| 13514   | PRIYANK PATHAK                      | Monday    | Abhishek Rose      |
| 13546   | RAJNESH KUMAR MEENA                 | Monday    |                    |
| 13755   | UTKARSH GUPTA                       | Monday    | Shishir Mathur     |
| 14013   | ABHINAV GARG                        | Monday    | Susmit Wagle       |
| 14021   | ABHISHEK MAHER                      | Monday    | Rajat Jain         |
| 14026   | ABHISHEK VERMA                      | Monday    | Susmit Wagle       |
| 14031   | ADITYA GAJBHIYE                     | Monday    | Rajat Jain         |
| 14050   | AKAVARAPU VENKATA SATYA DURGA SAI M | Monday    | Susmit Wagle       |
| 14055   | AKSHAT ANAND                        | Monday    | Shishir Mathur     |
| 14079   | AMBAVARAM PRADEEP KUMAR REDDY       | Wednesday |                    |
| 14080   | AMEYA RAJGOPAL LOYA                 | Monday    | Abhishek Rose      |
| 14097   | ANIL KUMAR GUPTA                    | Monday    | Shishir Mathur     |
| 14103   | ANKIT GUPTA                         | Monday    | Shishir Mathur     |
| 14109   | ANKUR KUMAR                         | Monday    | Rajat Jain         |
| 14114   | ANUBHAV SHRIVASTAVA                 | Monday    | Rajat Jain         |
| 14116   | ANUJ NAGPAL                         | Wednesday |                    |
| 14130   | ARHAM CHOPRA                        | Monday    | Shishir Mathur     |
| 14158   | AVINASH CHOUDHARY                   | Monday    | Susmit Wagle       |
| 14167   | AYUSH TULSYAN                       | Wednesday |                    |
| 14170   | BAJAJ RAHI DILIP                    | Monday    | Abhishek Rose      |
| 14173   | BHANGALE PRATIK ANIL                | Monday    | Susmit Wagle       |
| 14183   | BHUKYA ANANTH                       | Wednesday |                    |
| 14189   | BORKAR PRANAY SHRIHARI              | Monday    | Mohd Salman Khan   |
| 14191   | CHAKALA AMARNATH                    | Monday    | Mohd Salman Khan   |
| 14195   | CHAUHAN MAYURDHWAJSINHJI NITIRAJ    | Monday    | Abhishek Rose      |
| 14214   | DEVENDRA K MEENA                    | Monday    | Shishir Mathur     |
| 14218   | DHAWAL UPADHYAY                     | Monday    | Shishir Mathur     |
| 14246   | GHANTUDIYA NIKHIL DIPAKKUMAR        | Wednesday |                    |
| 14249   | GOPICHAND KOTANA                    | Monday    | Susmit Wagle       |
| 14254   | GOURAV KATHA                        | Wednesday |                    |
| 14257   | GUNDA ABHISHEK                      | Monday    | Mohd Salman Khan   |
| 14258   | GURKIRAT SINGH                      | Monday    | Abhishek Rose      |
| 14259   | GYANESH GUPTA                       | Monday    | Susmit Wagle       |
| 14275   | ISHIKA SONI                         | Monday    | Shishir Mathur     |
| 14282   | JAYANT AGRAWAL                      | Monday    | Rajat Jain         |
| 14285   | JEENU SRI JASWANTH CHANDRA          | Monday    | Mohd Salman Khan   |
| 14292   | JONNALA KRISHNA KARTHIK REDDY       | Monday    | Mohd Salman Khan   |
| 14299   | KAMLESH KUMAR MEENA                 | Wednesday |                    |
| 14305   | KARTIK                              | Monday    | Abhishek Rose      |
| 14308   | KARTIK RAJ                          | Monday    | Rajat Jain         |
| 14313   | KAUSHAL AGRAWAL                     | Monday    | Rajat Jain         |
| 14314   | KAUSTUBH VIVEK RANE                 | Monday    | Mohd Salman Khan   |
| 14320   | KISHAN GOPAL                        | Monday    | Abhishek Rose      |
| 14326   | KRITAGYA DABI                       | Monday    | Shishir Mathur     |
| 14330   | KSHIITIZ SINGHAL                    | Monday    | Mohd Salman Khan   |
| 14333   | KSHITIZ SUMAN                       | Monday    | Rajat Jain         |
| 14342   | KUNAL CHATURVEDI                    | Monday    | Susmit Wagle       |
| 14371   | MANOJ GHORELA                       | Wednesday |                    |
| 14377   | MAYANK PATEL                        | Wednesday |                    |
| 14384   | MEENA VIKAS GAJANAND                | Wednesday |                    |
| 14388   | MOHAMMAD AFROZ ALAM                 | Wednesday | Debjeet Majumdar   |
| 14402   | MUKKU ROHITH                        | Wednesday |                    |
| 14405   | MUNOT RUSHAB PREETAM                | Wednesday | Debjeet Majumdar   |
| 14408   | NALLURU SAI HARSHA                  | Wednesday |                    |
| 14418   | NAYAN DESHMUKH                      | Wednesday |                    |
| 14429   | NIKHIL VANJANI                      | Wednesday |                    |
| 14433   | NISHIT ASNANI                       | Wednesday |                    |
| 14441   | P SUGHOSH                           | Wednesday |                    |
| 14443   | PALLAV AGARWAL                      | Wednesday | Debjeet Majumdar   |
| 14449   | PARTH SHARMA                        | Wednesday |                    |
| 14453   | PAWAN KUMAR PATEL                   | Wednesday |                    |
| 14466   | POGIRI KALA SAGAR                   | Wednesday |                    |
| 14468   | PONDUGALA SAI SURYA VAMSI BHARGAVA  | Wednesday |                    |
| 14506   | PUNEET KUMAR VERMA                  | Wednesday |                    |
| 14511   | R ANIMESH                           | Wednesday |                    |
| 14519   | RAHUL GUPTA                         | Wednesday |                    |
| 14548   | RISHABH BHARDWAJ                    | Wednesday |                    |
| 14549   | RISHABH GOYAL                       | Wednesday | Debjeet Majumdar   |
| 14575   | SACHIN K SALIM                      | Wednesday | Debjeet Majumdar   |
| 14588   | SAKSHAM SHARMA                      | Wednesday |                    |
| 14590   | SAMICK BISWAS                       | Wednesday |                    |
| 14599   | SANJAY BHARAT                       | Wednesday | Debjeet Majumdar   |
| 14601   | SANJAY RAMESH                       | Wednesday |                    |
| 14606   | SARTHAK PATTANAYAK                  | Wednesday |                    |
| 14624   | SHAHZEB HAIDAR                      | Wednesday |                    |
| 14629   | SHARMA SHUBHAM MANGILAL             | Wednesday |                    |
| 14632   | SHASHAANK KAIM                      | Wednesday |                    |
| 14644   | SHIBHANSH DOHARE                    | Wednesday |                    |
| 14655   | SHIVAM YADAV                        | Wednesday |                    |
| 14668   | SHRUTI AGRAWAL                      | Wednesday |                    |
| 14679   | SHUBHAM KUMAR PANDEY                | Wednesday |                    |
| 14708   | SRISHTI JAIN                        | Wednesday | Debjeet Majumdar   |
| 14716   | SUDHIR KUMAR                        | Wednesday |                    |
| 14745   | TAJI SHIVAM MANOJ                   | Wednesday | Debjeet Majumdar   |
| 14746   | TALLA ARAVIND REDDY                 | Monday    | Mohd Salman Khan   |
| 14762   | TUSHANT MITTAL                      | Wednesday |                    |
| 14766   | TUSHAR VATSAL                       | Wednesday |                    |
| 14767   | UDDESHYA PATIL                      | Wednesday |                    |
| 14784   | VADISETTI GOWTHAM PRUDHVI           | Wednesday |                    |
| 14785   | VAIBHAV NAGAR                       | Wednesday |                    |
| 14793   | VIBHOR GOYAL                        | Wednesday |                    |
| 14805   | VINAYAK TANTIA                      | Wednesday | Debjeet Majumdar   |
| 14809   | VISHAD GOYAL                        | Wednesday |                    |
| 14826   | YEDURU VINOD KUMAR REDDY            | Wednesday |                    |
| 14830   | YOGESH KUMAR CHAUHAN                | Wednesday |                    |
